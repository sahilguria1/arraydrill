export function reduce(elements, cb, startingValue) {

  //Array check
  if (!Array.isArray(elements)) {
    throw new Error("input type is not array");
  }

  let accumulator = startingValue;
  let startIndex = 0;

  if (startingValue === undefined) {
    startIndex = 1;
    accumulator = elements[0];
  }

  for (let index = startIndex; index < elements.length; index++) {
    accumulator = cb(startingValue, elements[index]);
  }
  return accumulator;
}
