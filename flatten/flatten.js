export function flatten(elements) {
     //Array check
    if (!Array.isArray(elements)) {
        throw new Error("input type is not array");
    }

    let resultArray = [];

    function recursionHelper(arr){
        for(let index = 0; index<arr.length; index++){
            if(Array.isArray(arr[index])){
                recursionHelper(arr[index]);
            }else{
                resultArray.push(arr[index]);
            }
        }
    }

    recursionHelper(elements);
    return  resultArray;

}