export function map(elements, cb) {
  //Array check
  if (!Array.isArray(elements)) {
    throw new Error("input type is not array");
  }
  if(typeof cb !== 'function'){
      throw new Error("second argument should be function")
  }

  //storing the output data
  const result = [];

  //calling cb function for all index and storing it on result
  for (let index = 0; index < elements.length; index++) {
    result.push(cb(elements[index]));
  }

  // Return the new array with the mapped values
  return result;
}
