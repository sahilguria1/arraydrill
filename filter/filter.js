export function filter(elements, cb) {
  //Array check
  if (!Array.isArray(elements)) {
    throw new Error("input type is not array");
  }

  let resultArray = [];

  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index])) {
      resultArray.push(elements[index]);
    }
  }

  return resultArray;
}
