export function find(elements, cb) {
  //Array check
  if (!Array.isArray(elements)) {
    throw new Error("input type is not array");
  }

  //Assigning task to cb function by every element
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index])) {
      return elements[index];
    }
  }

  return undefined;
}
